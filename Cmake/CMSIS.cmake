####################################################################################################
##
## CMAKE FILE
##--------------------------------------------------------------------------------------------------
##
## File         : CMSIS.cmake
## Author       : Erhan Meral
## Project      : Orhan - WCB (Wheelchair Control Box)
## Description  : Target microcontroller = STM32F407VGT6 (ARM CORTEX-M4 with FPU)
##
##
####################################################################################################

set(CMSIS_SOURCES 
   
	# Start-Up File
	${CMSIS_PATH}/Device/ST/STM32F4xx/Source/Templates/gcc/startup_stm32f407xx.s


    ${CMSIS_PATH}/Device/ST/STM32F4xx/Source/Templates/system_stm32f4xx.c
	
   )
   
set(CMSIS_INCLUDES
 
    ${CMSIS_PATH}/Include/
    ${CMSIS_PATH}/Device/ST/STM32F4xx/Include/

   )
