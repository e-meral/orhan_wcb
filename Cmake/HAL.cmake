####################################################################################################
##
## CMAKE FILE
##--------------------------------------------------------------------------------------------------
##
## File         : HAL.cmake
## Author       : Erhan Meral
## Project      : Orhan - WCB (Wheelchair Control Box)
## Description  : 
##
##
####################################################################################################

set(HAL_SOURCES

    # Click boards
	${CMAKE_SOURCE_DIR}/HAL/click_boards/pg_pilotplus_click.c
	${CMAKE_SOURCE_DIR}/HAL/click_boards/mikroe_buzz_click.c
    ${CMAKE_SOURCE_DIR}/HAL/click_boards/mikroe_ble_p_click.c

# TODO: Gyro and BLE support must be implemented 
#	${CMAKE_SOURCE_DIR}/HAL/click_boards/mikroe_gyro_click.c
#	${CMAKE_SOURCE_DIR}/HAL/click_boards/mikroe_ble_p_click.c

	${CMAKE_SOURCE_DIR}/HAL/hal_stm32f4discovery_board.c
	${CMAKE_SOURCE_DIR}/HAL/pg_pilotplus_communication.c
	${CMAKE_SOURCE_DIR}/HAL/hal_clock.c
	${CMAKE_SOURCE_DIR}/HAL/hal_error.c

   )

set(HAL_INCLUDES

	${CMAKE_SOURCE_DIR}/HAL/
	${CMAKE_SOURCE_DIR}/HAL/click_boards/

   )
