####################################################################################################
##
## CMAKE FILE
##--------------------------------------------------------------------------------------------------
##
## File         : Components.cmake
## Author       : Erhan Meral
## Project      : Orhan - WCB (Wheelchair Control Box)
## Description  : Target microcontroller = STM32F407VGT6 (ARM CORTEX-M4 with FPU)
##
##
####################################################################################################

set(COMPONENTS_SOURCES 

	# Components
	${STMicroelectronics_PATH}/Drivers/BSP/Components/lis302dl/lis302dl.c

   )

set(COMPONENTS_INCLUDES 

	# Components
	${STMicroelectronics_PATH}/Drivers/BSP/Components/lis302dl/

   )
