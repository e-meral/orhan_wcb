####################################################################################################
##
## CMAKE FILE
##--------------------------------------------------------------------------------------------------
##
## File         : APP.cmake
## Author       : Erhan Meral
## Project      : Orhan - WCB (Wheelchair Control Box)
## Description  : 
##
##
####################################################################################################

set(APP_SOURCES

	${CMAKE_SOURCE_DIR}/APP/main.c
	${CMAKE_SOURCE_DIR}/APP/app_buzzer.c
	${CMAKE_SOURCE_DIR}/APP/app_driver_control.c
	${CMAKE_SOURCE_DIR}/APP/app_heartbeat.c
	${CMAKE_SOURCE_DIR}/APP/app_joystick_control.c

	CACHE INTERNAL "app src"

   )

set(APP_INCLUDES

	${CMAKE_SOURCE_DIR}/APP/

   )
