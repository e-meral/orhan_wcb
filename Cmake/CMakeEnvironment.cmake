####################################################################################################
##
## CMAKE FILE
##--------------------------------------------------------------------------------------------------
##
## File         : CMakeEnvironment.cmake
## Author       : Erhan Meral
## Project      : Orhan - WCB (Wheelchair Control Box)
## Description  : 
##
##
####################################################################################################

## Toolchain, ARM GCC (GNU Tools for Arm Embedded Processors 9-2019-q4-major) Version: 9.2.1
## Please change this path depending on where your toolchain has been installed.
set(ARM_NONE_EABI_TOOLCHAIN_PATH "${CMAKE_SOURCE_DIR}/../gcc-arm-none-eabi-9-2019-q4-major/bin/")

## Configurations
set(CONFIGURATIONS_PATH "${CMAKE_SOURCE_DIR}/Configurations")

## ThirdParty

# STMicroelectronics, STM32Cube FW F4 Version: 1.25.0 
set(STMicroelectronics_PATH "${CMAKE_SOURCE_DIR}/ThirdParty/STMicroelectronics/STM32Cube_FW_F4_V1.25.0")

# CMSIS files are included in STM32Cube FW F4 Version: 1.25.0 folder
set(CMSIS_PATH "${CMAKE_SOURCE_DIR}/ThirdParty/STMicroelectronics/STM32Cube_FW_F4_V1.25.0/Drivers/CMSIS")

# FreeRTOS Version: 10.3.1
set(FREERTOS_PATH "${CMAKE_SOURCE_DIR}/ThirdParty/FreeRTOS/FreeRTOSv10.3.1")






