####################################################################################################
##
## CMAKE FILE
##--------------------------------------------------------------------------------------------------
##
## File         : Toolchain.cmake
## Author       : Erhan Meral
## Project      : Orhan - WCB (Wheelchair Control Box)
## Description  : Toolchain = ARM GCC
##              : Target microcontroller = STM32F407VGT6 (ARM CORTEX-M4 with FPU)
##
##
####################################################################################################

# Language standard/version settings
set(CMAKE_C_STANDARD 11)
set(CMAKE_CXX_STANDARD 14)

set(CMAKE_SYSTEM_NAME Generic)
set(CMAKE_SYSTEM_VERSION 1)
set(CMAKE_SYSTEM_PROCESSOR arm)

# Configure cmake to use the arm-none-eabi-gcc
set(CMAKE_TRY_COMPILE_TARGET_TYPE STATIC_LIBRARY)
set(CMAKE_ASM_COMPILER arm-none-eabi-gcc)
set(CMAKE_C_COMPILER arm-none-eabi-gcc)
set(CMAKE_CXX_COMPILER arm-none-eabi-g++)
set(CMAKE_LINKER arm-none-eabi-ld)

set(MY_SIZE arm-none-eabi-size)
set(MY_OBJCOPY arm-none-eabi-objcopy)

set(CMAKE_FIND_ROOT_PATH_MODE_LIBRARY ONLY)
set(CMAKE_FIND_ROOT_PATH_MODE_INCLUDE ONLY)

set(STM32_LINKER_SCRIPT "${CMAKE_SOURCE_DIR}/ThirdParty/STMicroelectronics/Linker/STM32F407VGTx_FLASH.ld")

add_definitions("-DUSE_STDPERIPH_DRIVER -DSTM32F407xx -DSTM32F40_41xxx -DHSE_VALUE=8000000U -DARM_MATH_CM4")

set(CFLAGS "${CFLAGS} -MP -MD -Wall -Werror -Os -g0 -ffunction-sections -fdata-sections -fno-strict-aliasing -fno-builtin --short-enums ")
set(CPU_FLAGS "-mcpu=cortex-m4 -mthumb -mfloat-abi=hard -mfpu=fpv4-sp-d16 -fsingle-precision-constant -mabi=aapcs -finline-functions -Wdouble-promotion -std=gnu99")
set(ASM_FLAGS "-x assembler-with-cpp")

set(LINKER_FLAGS "${CPU_FLAGS} -T${STM32_LINKER_SCRIPT} -Wl,--gc-sections,-Map,output.map --specs=nano.specs -specs=nosys.specs -lc -lnosys")

# Compiler/assambler/linker flags
set(CMAKE_C_FLAGS "${C_FLAGS} ${CPU_FLAGS}")
set(CMAKE_CXX_FLAGS "${C_FLAGS} ${CPU_FLAGS}")
set(CMAKE_ASM_FLAGS "${ASM_FLAGS} ${CPU_FLAGS}")
set(CMAKE_EXE_LINKER_FLAGS "${LINKER_FLAGS}")

macro(createHex EX_NAME)
	add_custom_command(TARGET ${EX_NAME} POST_BUILD
		COMMAND ${MY_SIZE} ${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/${EX_NAME}.out
        COMMAND ${MY_OBJCOPY} -O ihex ${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/${EX_NAME}.out "${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/${EX_NAME}.hex"
		COMMENT "Post build steps for ${EX_NAME}"
		)
endmacro()
