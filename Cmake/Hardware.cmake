####################################################################################################
##
## CMAKE FILE
##--------------------------------------------------------------------------------------------------
##
## File         : Hardware.cmake
## Author       : Erhan Meral
## Project      : Orhan - WCB (Wheelchair Control Box)
## Description  : Target microcontroller = STM32F407VGT6 (ARM CORTEX-M4 with FPU)
##
##
####################################################################################################

set(HARDWARE_SOURCES 
   
	${HARDWARE_PATH}/startup_stm32f4xx.s
    ${HARDWARE_PATH}/system_stm32f4xx.c
	${HARDWARE_PATH}/stm32f4xx_it.c

   )

set(HARDWARE_INCLUDES
   
	${HARDWARE_PATH}/

   )
	
