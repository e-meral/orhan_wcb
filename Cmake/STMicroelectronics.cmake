####################################################################################################
##
## CMAKE FILE
##--------------------------------------------------------------------------------------------------
##
## File         : STMicroelectronics.cmake
## Author       : Erhan Meral
## Project      : Orhan - WCB (Wheelchair Control Box)
## Description  : Target microcontroller = STM32F407VGT6 (ARM CORTEX-M4 with FPU)
##
##
####################################################################################################

set(STM32CUBE_SOURCES 

    # BSP
    ${STMicroelectronics_PATH}/Drivers/BSP/STM32F4-Discovery/stm32f4_discovery.c
    ${STMicroelectronics_PATH}/Drivers/BSP/STM32F4-Discovery/stm32f4_discovery_accelerometer.c
    ${STMicroelectronics_PATH}/Drivers/BSP/STM32F4-Discovery/stm32f4_discovery_audio.c

    # HAL
    ${STMicroelectronics_PATH}/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal.c
    ${STMicroelectronics_PATH}/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_adc.c
    ${STMicroelectronics_PATH}/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_adc_ex.c
    ${STMicroelectronics_PATH}/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_can.c
    ${STMicroelectronics_PATH}/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_cortex.c
    ${STMicroelectronics_PATH}/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_crc.c
    ${STMicroelectronics_PATH}/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_cryp.c
    ${STMicroelectronics_PATH}/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_cryp_ex.c
    ${STMicroelectronics_PATH}/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_dac.c
    ${STMicroelectronics_PATH}/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_dac_ex.c
    ${STMicroelectronics_PATH}/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_dcmi.c
    ${STMicroelectronics_PATH}/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_dma.c
    ${STMicroelectronics_PATH}/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_dma2d.c
    ${STMicroelectronics_PATH}/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_dma_ex.c
    ${STMicroelectronics_PATH}/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_eth.c
    ${STMicroelectronics_PATH}/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_flash.c
    ${STMicroelectronics_PATH}/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_flash_ex.c
    ${STMicroelectronics_PATH}/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_flash_ramfunc.c
    ${STMicroelectronics_PATH}/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_gpio.c
    ${STMicroelectronics_PATH}/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_hash.c
    ${STMicroelectronics_PATH}/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_hcd.c
    ${STMicroelectronics_PATH}/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_i2c.c
    ${STMicroelectronics_PATH}/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_i2c_ex.c
    ${STMicroelectronics_PATH}/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_i2s.c
    ${STMicroelectronics_PATH}/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_i2s_ex.c
    ${STMicroelectronics_PATH}/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_irda.c
    ${STMicroelectronics_PATH}/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_iwdg.c
    ${STMicroelectronics_PATH}/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_ltdc.c
    ${STMicroelectronics_PATH}/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_msp_template.c
    ${STMicroelectronics_PATH}/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_nand.c
    ${STMicroelectronics_PATH}/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_nor.c
    ${STMicroelectronics_PATH}/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_pccard.c
    ${STMicroelectronics_PATH}/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_pcd.c
    ${STMicroelectronics_PATH}/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_pcd_ex.c
    ${STMicroelectronics_PATH}/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_pwr.c
    ${STMicroelectronics_PATH}/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_pwr_ex.c
    ${STMicroelectronics_PATH}/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_rcc.c
    ${STMicroelectronics_PATH}/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_rcc_ex.c
    ${STMicroelectronics_PATH}/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_rng.c
    ${STMicroelectronics_PATH}/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_rtc.c
    ${STMicroelectronics_PATH}/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_rtc_ex.c
    ${STMicroelectronics_PATH}/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_sai.c
    ${STMicroelectronics_PATH}/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_sd.c
    ${STMicroelectronics_PATH}/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_sdram.c
    ${STMicroelectronics_PATH}/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_smartcard.c
    ${STMicroelectronics_PATH}/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_spi.c
    ${STMicroelectronics_PATH}/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_sram.c
    ${STMicroelectronics_PATH}/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_tim.c
    ${STMicroelectronics_PATH}/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_tim_ex.c
    ${STMicroelectronics_PATH}/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_uart.c
    ${STMicroelectronics_PATH}/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_usart.c
    ${STMicroelectronics_PATH}/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_wwdg.c
    ${STMicroelectronics_PATH}/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_ll_fmc.c
    ${STMicroelectronics_PATH}/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_ll_fsmc.c
    ${STMicroelectronics_PATH}/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_ll_sdmmc.c
    ${STMicroelectronics_PATH}/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_ll_usb.c

    # STM32 USB Host Library
    #${STMicroelectronics_PATH}/Middlewares/ST/STM32_USB_Host_Library/Class/HID/Src/usbh_hid.c
    #${STMicroelectronics_PATH}/Middlewares/ST/STM32_USB_Host_Library/Class/HID/Src/usbh_hid_keybd.c
    #${STMicroelectronics_PATH}/Middlewares/ST/STM32_USB_Host_Library/Class/HID/Src/usbh_hid_mouse.c
    #${STMicroelectronics_PATH}/Middlewares/ST/STM32_USB_Host_Library/Class/HID/Src/usbh_hid_parser.c

    # HAL CAN-BUS
    ${STMicroelectronics_PATH}/Drivers/STM32F4xx_HAL_Driver/Src/Legacy/stm32f4xx_hal_can.c

   )
   

set(STM32CUBE_INCLUDES 

    ${STMicroelectronics_PATH}/Drivers/BSP/STM32F4-Discovery/
    ${STMicroelectronics_PATH}/Drivers/STM32F4xx_HAL_Driver/Inc/
    ${STMicroelectronics_PATH}/Drivers/STM32F4xx_HAL_Driver/Inc/Legacy/
    ${STMicroelectronics_PATH}/Middlewares/ST/STM32_USB_Host_Library/Class/HID/Inc/
   )


