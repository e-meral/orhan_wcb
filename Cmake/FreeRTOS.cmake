####################################################################################################
##
## CMAKE FILE
##--------------------------------------------------------------------------------------------------
##
## File         : FreeRTOS.cmake
## Author       : Erhan Meral
## Project      : Orhan - WCB (Wheelchair Control Box)
## Description  : FreeRTOS Version = 10.3.1
##
##
####################################################################################################

set(FREERTOS_SOURCES 
   
	${FREERTOS_PATH}/FreeRTOS/Source/portable/GCC/ARM_CM4F/port.c
    ${FREERTOS_PATH}/FreeRTOS/Source/list.c	
    ${FREERTOS_PATH}/FreeRTOS/Source/queue.c
    ${FREERTOS_PATH}/FreeRTOS/Source/tasks.c
    ${FREERTOS_PATH}/FreeRTOS/Source/event_groups.c
    ${FREERTOS_PATH}/FreeRTOS/Source/timers.c
	${FREERTOS_PATH}/FreeRTOS/Source/portable/MemMang/heap_1.c
	
   )
   
set(FREERTOS_INCLUDES

	${FREERTOS_PATH}/FreeRTOS/Source/include/
    ${FREERTOS_PATH}/FreeRTOS/Source/portable/GCC/ARM_CM4F/

	# Also the FreeRTOSConfig.h file path must be included. This file is not located
	# within the FreeRTOS folder due to it is a part of the application.
	# Must be included at application side.. -> "Configuration.cmake"

   )
