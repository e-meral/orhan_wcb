# Project: Orhan - WCB (Wheelchair Control Box)

## Description
Project "Orhan" is focusing on custom control possibilities for electric powered wheelchairs. This because not everybody can drive the wheelchair by onboard standard joystick (depending on the situation of the disabled person). "Orhan" is the general name of the project which can have multiple hardware devices to achieve a goal together: Controlling the wheelchair.

This part is so called the "Wheelchair Control Box", which is the joystick replacer. The WCB is responsible for:

* Communicating with the "Motor Control Box"
* Reading inputs from driver
* Supports CAN-Bus communication to act as a slave
* Supports Bluetooth Low Energy (BLE) communication so the wheelchair can be controlled by PC/smartphone/tablet

## Hardware
For now the project has been started with the following hardware:

* STM32F4DISCOVERY development board from ST Microelectronics. It contains a ARM Cortex-M4 microcontroller.
* The STM32FDISCOVERY development board has been click on top of a main-shield board from Mikroe: "STM32F4 DISCOVERY SHIELD". This shield-board enables 4 so-called click-boards that can be mounted also on top of the shield board. For this project the following click-boards has been used:
    1. Gyro click [from Mikroe]: the hardware will be mounted on the seat-back of the wheelchair which can be tilt, with the gyro the angle can be measured/monitored
    2. Buzz click [from Mikroe]: for power-on beep and pulsing beep during reverse driving
    3. BLE P click [from Mikroe]: Bluetooth Low Energy for PC/smartphone/tablet control
    4. Custom made click board to handle the input signals from the driver

![](Documentation/HardwareBlockDiagram.png "Hardware Block Diagram")

## Software
The software for this CMAKE-project has been written in C/C++. Clone and build instructions:

First of all, clone the project:
```
git clone https://gitlab.com/e-meral/orhan_wcb.git
```
After cloning, navigate to the project folder and create a new folder: "build". Inside that build folder run CMake:
```
cmake ..
```
Last step is to building the project with the following command:
```
make -j4
```

## Note
Project "Orhan" is a educative hobby project and comes with ABSOLUTELY NO WARRANTY, to the extent permitted by applicable law.

## License



