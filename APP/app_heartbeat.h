/***************************************************************************************************
**
** File         : app_heartbeat.h
** Created on   :
** Author       : Erhan Meral
** Project      : Orhan - WCB (Wheelchair Control Box)
** Description  :
**
**
***************************************************************************************************/


#ifndef __APP_HEARTBEAT_H_
#define __APP_HEARTBEAT_H_

// Function protoypes ******************************************************************************


void blink_led(void *pvParameters);
void STM_EVAL_LEDToggle(Led_TypeDef2 Led);


#endif
