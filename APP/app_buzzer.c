/***************************************************************************************************
**
** File         : app_buzzer.c
** Created on   :
** Author       : Erhan Meral
** Project      : Orhan - WCB (Wheelchair Control Box)
** Description  :
**
**
***************************************************************************************************/

// Includes
#include <stdbool.h>
#include <string.h>
#include "stm32f4xx_hal.h"
#include "FreeRTOS.h"
#include "task.h"
#include "stm32f4xx_hal_uart.h"
#include "stm32f4xx_hal_gpio.h"
#include "mikroe_buzz_click.h"
#include "app_buzzer.h"


void play_poweron_sound(void *pvParameters)
{
    for(;;)
	{
        start_piezo_sound();
        vTaskDelay( 100 / portTICK_RATE_MS );
        stop_piezo_sound();

        vTaskDelay( 50 / portTICK_RATE_MS );

        start_piezo_sound();
        vTaskDelay( 100 / portTICK_RATE_MS );
        stop_piezo_sound();

        vTaskSuspend(NULL);

    /***********************************************************
    TODO    : vPlayPowerOnSound is function that must be excecuted
            : only once at startup. Then it can be deleted, in this
            : case deleting this task with the code below results in
            : a freezing FreeRTOS.. So for now this task will be suspended.
            : That works! Should be checked later..


        vTaskSuspendAll();

        if( ptr_power_on_task != NULL )
        {
            TaskHandle_t tmp = ptr_power_on_task;           

            // The task is going to be deleted.
            // Set the handle to NULL.
            ptr_power_on_task = NULL;

            // Delete using the copy of the handle.
            vTaskDelete( tmp );
        }
        xTaskResumeAll();       
    ***********************************************************/

    }
}
