/***************************************************************************************************
**
** File         : main.c
** Created on   :
** Author       : Erhan Meral
** Project      : Orhan - WCB (Wheelchair Control Box)
** Description  :
**
**
***************************************************************************************************/

// Includes ----------------------------------------------------------------------------------------
#include <stdbool.h>
#include "stm32f4xx.h"
#include "hal_clock.h"
#include "stm32f4_discovery.h"
#include "FreeRTOS.h"
#include "task.h"
#include "math.h"
#include "stdio.h"
#include "stm32f4xx_hal.h"
#include "main.h"
#include "hal_stm32f4discovery_board.h"
#include "app_heartbeat.h"
#include "app_buzzer.h"
#include "app_joystick_control.h"
#include "app_driver_control.h"
#include "mikroe_buzz_click.h"
#include "pg_pilotplus_click.h"
#include "pg_pilotplus_communication.h"

// Defines -----------------------------------------------------------------------------------------

// Macro to use CCM (Core Coupled Memory) in STM32F4
#define CCM_RAM __attribute__((section(".ccmram")))
#define FPU_TASK_STACK_SIZE 256
#define STACK_SIZE_MIN	128	// usStackDepth	- the stack size DEFINED IN WORDS.


// Globals -----------------------------------------------------------------------------------------

StackType_t fpuTaskStack[FPU_TASK_STACK_SIZE] CCM_RAM;  // Put task stack in CCM
StaticTask_t fpuTaskBuffer CCM_RAM;  // Put TCB in CCM

TaskHandle_t ptr_power_on_task = NULL;


// Function prototypes -----------------------------------------------------------------------------

static void MX_GPIO_Init(void);


// Start main function -----------------------------------------------------------------------------
int main(void)
{
    // Reset of all peripherals, initializes the flash interface and the systick. 
    HAL_Init();

    init_system_clock();

    init_stm32f4discovery_board();

    // Init click boards
    init_buzz_click();

    //init_joystick();

    init_pg_pilotplus_click();

    init_pg_pilotplus_communication();

    // Create a task
    // Stack and TCB are placed in CCM of STM32F4
    // The CCM block is connected directly to the core, which leads to zero wait states

    xTaskCreate( play_poweron_sound, (const char*)"Power On Sound Task", STACK_SIZE_MIN, NULL, tskIDLE_PRIORITY+2, &ptr_power_on_task );
    xTaskCreate( blink_led, (const char*)"Led Blink Task", STACK_SIZE_MIN, NULL, tskIDLE_PRIORITY+1, NULL );
    xTaskCreate( vHandleDriverInput, (const char*)"Handle Driver Input Task", STACK_SIZE_MIN, NULL, tskIDLE_PRIORITY+1, NULL );
    xTaskCreate( handle_drive_data, (const char*)"Handle Drive Data Task", STACK_SIZE_MIN, NULL, tskIDLE_PRIORITY+1, NULL );
    //xTaskCreate( handle_joystick, (const char*)"Handle Joystick Communication Task", STACK_SIZE_MIN, NULL, tskIDLE_PRIORITY+1, NULL );




    vTaskStartScheduler();  // should never return

    while(true)
    {
        
    }
}
// End main function -------------------------------------------------------------------------------


void vApplicationTickHook(void) {
}

/* 
    vApplicationMallocFailedHook() will only be called if
    configUSE_MALLOC_FAILED_HOOK is set to 1 in FreeRTOSConfig.h.  It is a hook
    function that will get called if a call to pvPortMalloc() fails.
    pvPortMalloc() is called internally by the kernel whenever a task, queue,
    timer or semaphore is created.  It is also called by various parts of the
    demo application.  If heap_1.c or heap_2.c are used, then the size of the
    heap available to pvPortMalloc() is defined by configTOTAL_HEAP_SIZE in
    FreeRTOSConfig.h, and the xPortGetFreeHeapSize() API function can be used
    to query the size of free heap space that remains (although it does not
    provide information on how the remaining heap might be fragmented). 
*/
void vApplicationMallocFailedHook(void) 
{
    taskDISABLE_INTERRUPTS();
    for(;;);        
}


/* 
    vApplicationIdleHook() will only be called if configUSE_IDLE_HOOK is set
    to 1 in FreeRTOSConfig.h.  It will be called on each iteration of the idle
    task.  It is essential that code added to this hook function never attempts
    to block in any way (for example, call xQueueReceive() with a block time
    specified, or call vTaskDelay()).  If the application makes use of the
    vTaskDelete() API function (as this demo application does) then it is also
    important that vApplicationIdleHook() is permitted to return to its calling
    function, because it is the responsibility of the idle task to clean up
    memory allocated by the kernel to any task that has since been deleted.
*/
void vApplicationIdleHook(void)
{

}


void vApplicationStackOverflowHook(xTaskHandle pxTask, signed char *pcTaskName)
{
    (void) pcTaskName;
    (void) pxTask;
    // Run time stack overflow checking is performed if
    // configCHECK_FOR_STACK_OVERFLOW is defined to 1 or 2.  This hook
    // function is called if a stack overflow is detected.
    taskDISABLE_INTERRUPTS();
    for(;;);
}

StaticTask_t xIdleTaskTCB CCM_RAM;
StackType_t uxIdleTaskStack[configMINIMAL_STACK_SIZE] CCM_RAM;

/*
    configUSE_STATIC_ALLOCATION is set to 1, so the application must provide an
    implementation of vApplicationGetIdleTaskMemory() to provide the memory that is
    used by the Idle task.
*/
void vApplicationGetIdleTaskMemory(StaticTask_t **ppxIdleTaskTCBBuffer, StackType_t **ppxIdleTaskStackBuffer, uint32_t *pulIdleTaskStackSize)
{
  /* Pass out a pointer to the StaticTask_t structure in which the Idle task's
  state will be stored. */
  *ppxIdleTaskTCBBuffer = &xIdleTaskTCB;

  /* Pass out the array that will be used as the Idle task's stack. */
  *ppxIdleTaskStackBuffer = uxIdleTaskStack;

  /* Pass out the size of the array pointed to by *ppxIdleTaskStackBuffer.
  Note that, as the array is necessarily of type StackType_t,
  configMINIMAL_STACK_SIZE is specified in words, not bytes. */
  *pulIdleTaskStackSize = configMINIMAL_STACK_SIZE;
}

static StaticTask_t xTimerTaskTCB CCM_RAM;
static StackType_t uxTimerTaskStack[configTIMER_TASK_STACK_DEPTH] CCM_RAM;


/*
    configUSE_STATIC_ALLOCATION and configUSE_TIMERS are both set to 1, so the
    application must provide an implementation of vApplicationGetTimerTaskMemory()
    to provide the memory that is used by the Timer service task.
*/
void vApplicationGetTimerTaskMemory(StaticTask_t **ppxTimerTaskTCBBuffer, StackType_t **ppxTimerTaskStackBuffer, uint32_t *pulTimerTaskStackSize)
{
    *ppxTimerTaskTCBBuffer = &xTimerTaskTCB;
    *ppxTimerTaskStackBuffer = uxTimerTaskStack;
    *pulTimerTaskStackSize = configTIMER_TASK_STACK_DEPTH;
}
