/***************************************************************************************************
**
** File         : version.h
** Created on   :
** Author       : Erhan Meral
** Project      : Orhan - WCB (Wheelchair Control Box)
** Description  : 
**
***************************************************************************************************/

#ifndef __VERSION_H__
#define __VERSION_H__

// Defines *****************************************************************************************

#define ORHAN_WCB_FW_VERSION    "1.0.0"

#endif
