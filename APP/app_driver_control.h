/***************************************************************************************************
**
** File         : app_driver_control.h
** Created on   :
** Author       : Erhan Meral
** Project      : Orhan - WCB (Wheelchair Control Box)
** Description  :
**
**
***************************************************************************************************/


#ifndef __APP_DRIVER_CONTROL_H_
#define __APP_DRIVER_CONTROL_H_

// Function protoypes ******************************************************************************

#define KEY_PRESSED     0x01
#define KEY_NOT_PRESSED 0x00

void handle_drive_data(void *pvParameters);
void vHandleDriverInput(void *pvParameters);

#endif
