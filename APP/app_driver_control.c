/***************************************************************************************************
**
** File         : app_heartbeat.c
** Created on   :
** Author       : Erhan Meral
** Project      : Orhan - WCB (Wheelchair Control Box)
** Description  :
**
**
***************************************************************************************************/

// Includes
#include <stdbool.h>
#include <string.h>
#include "FreeRTOS.h"
#include "task.h"
#include "stm32f4xx.h"
#include "stm32f4_discovery.h"
#include "stm32f4xx_hal.h"
#include "stm32f4xx_hal_uart.h"
#include "stm32f4xx_hal_gpio.h"
#include "mikroe_buzz_click.h"
#include "pg_pilotplus_click.h"
#include "pg_pilotplus_communication.h"
#include "app_driver_control.h"

void scan_driver_input(void);

void vHandleDriverInput(void *pvParameters)
{
    for(;;)
    {
        if(HAL_GPIO_ReadPin(FOOT_DIRECTION_PEDAL_PORT, FOOT_DIRECTION_PEDAL_PIN)==GPIO_PIN_RESET)
        //if(HAL_GPIO_ReadPin(HEAD_CONTROL_SWITCH_L_PORT, HEAD_CONTROL_SWITCH_L_PIN)==GPIO_PIN_RESET)       
        {
            start_piezo_sound();
            vTaskDelay( 500 / portTICK_RATE_MS );
            stop_piezo_sound();
            vTaskDelay( 500 / portTICK_RATE_MS );
        }    
    }
}
void scan_driver_input(void)
{
    if(HAL_GPIO_ReadPin(FOOT_ACCELERATION_PEDAL_PORT, FOOT_ACCELERATION_PEDAL_PIN)==GPIO_PIN_RESET) 
    {
        set_drive_level(100);
    }
    else if(HAL_GPIO_ReadPin(FOOT_DIRECTION_PEDAL_PORT, FOOT_DIRECTION_PEDAL_PIN)==GPIO_PIN_RESET)
    {
        set_drive_level(-75);
    }
    else if(HAL_GPIO_ReadPin(HEAD_CONTROL_SWITCH_L_PORT, HEAD_CONTROL_SWITCH_L_PIN)==GPIO_PIN_RESET) 
    {
        set_drive_level(0);
        set_turn_value(100);
    }
    else if(HAL_GPIO_ReadPin(HEAD_CONTROL_SWITCH_R_PORT, HEAD_CONTROL_SWITCH_R_PIN)==GPIO_PIN_RESET) 
    {
        set_drive_level(0);
        set_turn_value(-100);
    }
    else
    {
        set_drive_level(0);
        set_turn_value(0);
    }
}

void handle_drive_data(void *pvParameters)
{
    for(;;)
    {
        scan_driver_input();
        send_drive_data();
        vTaskDelay( 6 / portTICK_RATE_MS );
    }
}
