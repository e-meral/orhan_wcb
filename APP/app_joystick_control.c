/***************************************************************************************************
**
** File         : app_joystick_control.c
** Created on   :
** Author       : Erhan Meral
** Project      : Orhan - WCB (Wheelchair Control Box)
** Description  :
**
**
***************************************************************************************************/

// Includes
#include <stdbool.h>
#include <string.h>
#include "FreeRTOS.h"
#include "task.h"
#include "stm32f4xx.h"
#include "stm32f4_discovery.h"
#include "stm32f4xx_hal.h"
#include "stm32f4xx_hal_uart.h"
#include "hal_stm32f4discovery_board.h"
#include "app_heartbeat.h"
#include "app_joystick_control.h"

static UART_HandleTypeDef s_UARTHandle;

char get_joystick_data(void);

static uint8_t buffer;
static uart_ready = false;

void init_joystick(void)
{
    GPIO_InitTypeDef GPIO_InitStruct;

    // Peripheral clock enable
    __HAL_RCC_GPIOC_CLK_ENABLE();

    __USART6_CLK_ENABLE();

    GPIO_InitStruct.Pin = GPIO_PIN_7;
    GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_HIGH;
    GPIO_InitStruct.Alternate = GPIO_AF8_USART6;
    HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);


    // Baudrate: 115200, 8 databits and none
    s_UARTHandle.Instance        = USART6;              // USART3 is connected to the TX/RX pins on MikroBus connector 1
    s_UARTHandle.Init.BaudRate   = 115200;              // Baud rate
    s_UARTHandle.Init.WordLength = UART_WORDLENGTH_8B;  // WordLength must be 9 because of even-parity-bit 
    s_UARTHandle.Init.StopBits   = UART_STOPBITS_1;
    s_UARTHandle.Init.Parity     = UART_PARITY_NONE;
    s_UARTHandle.Init.HwFlowCtl  = UART_HWCONTROL_NONE;
    s_UARTHandle.Init.Mode       = UART_MODE_TX_RX;

    if (HAL_UART_Init(&s_UARTHandle) != HAL_OK)
    {
        asm("bkpt 255");
    }

    //NVIC_EnableIRQ(USART6_IRQn);
    //

}

void handle_joystick(void)
{
    for(;;)
    {  

        HAL_UART_Receive(&s_UARTHandle, &buffer, 1, 1);
        
        
        if('A' == (char)buffer)
        {
            STM_EVAL_LEDToggle(LED_GREEN);
        }
        else if('X' == (char)buffer)
        {
            STM_EVAL_LEDToggle(LED_BLUE);
        }
        else if('x' == (char)buffer)
        {
            STM_EVAL_LEDToggle(LED_BLUE);
        }
        else if('Y' == (char)buffer)
        {
            STM_EVAL_LEDToggle(LED_ORANGE);
        }
        else
        {
            
        }
        
        //uart_ready = false;
       
		
		vTaskDelay( 50 / portTICK_RATE_MS );

    }
}

/*
void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart) 
{
    uart_ready = true;

}*/

