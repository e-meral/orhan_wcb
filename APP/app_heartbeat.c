/***************************************************************************************************
**
** File         : app_heartbeat.c
** Created on   :
** Author       : Erhan Meral
** Project      : Orhan - WCB (Wheelchair Control Box)
** Description  :
**
**
***************************************************************************************************/

// Includes
#include <stdbool.h>
#include <string.h>
#include "FreeRTOS.h"
#include "task.h"
#include "stm32f4_discovery.h"
#include "stm32f4xx_hal.h"
#include "stm32f4xx_hal_uart.h"
#include "stm32f4xx_hal_gpio.h"
#include "hal_stm32f4discovery_board.h"
#include "app_heartbeat.h"

// Globals -----------------------------------------------------------------------------------------
//GPIO_TypeDef* GPIO_PORTS[LEDn] = {LED_GREEN_GPIO_PORT, LED_BLUE_GPIO_PORT, LED_RED_GPIO_PORT, LED_ORANGE_GPIO_PORT};
//const uint16_t GPIO_PINS[LEDn] = {LED_GREEN_PIN, LED_BLUE_PIN, LED_RED_PIN, LED_ORANGE_PIN};


GPIO_TypeDef* GPIO_PORTS[LEDn] = {LED_GREEN_GPIO_PORT, LED_BLUE_GPIO_PORT, LED_RED_GPIO_PORT, LED_ORANGE_GPIO_PORT};

const uint16_t GPIO_PINS[LEDn] = {LED_GREEN_PIN, LED_BLUE_PIN, LED_RED_PIN, LED_ORANGE_PIN};

// Functions *************************************************************************************** 

void blink_led(void *pvParameters)
{
	for(;;)
	{
		STM_EVAL_LEDToggle(LED_RED);
		vTaskDelay( 500 / portTICK_RATE_MS );
	}
}

void STM_EVAL_LEDToggle(Led_TypeDef2 Led)
{
    GPIO_PORTS[Led]->ODR ^= GPIO_PINS[Led]; 
}
