/***************************************************************************************************
**
** File         : hal_stm32f4discovery_board.c
** Created on   :
** Author       : Erhan Meral
** Project      : Orhan - WCB (Wheelchair Control Box)
** Description  :
**
**
***************************************************************************************************/

// Includes
#include <stdbool.h>
#include <string.h>
#include "stm32f4_discovery.h"
#include "stm32f4xx_hal.h"
#include "stm32f4xx_hal_uart.h"
#include "stm32f4xx_hal_gpio.h"
#include "hal_stm32f4discovery_board.h"

// Globals -----------------------------------------------------------------------------------------

//GPIO_TypeDef* GPIO_PORTS[LEDn] = {LED_GREEN_GPIO_PORT, LED_BLUE_GPIO_PORT, LED_RED_GPIO_PORT, LED_ORANGE_GPIO_PORT};
//const uint16_t GPIO_PINS[LEDn] = {LED_GREEN_PIN, LED_BLUE_PIN, LED_RED_PIN, LED_ORANGE_PIN};

void init_leds(void);
void init_push_button(void);


// Functions *************************************************************************************** 

void init_stm32f4discovery_board(void)
{
    init_leds();
    init_push_button();
}


void init_leds(void)
{
    // On board user LEDs are connecte to port D:
    // GREEN    = LED4 -> PD12 
    // ORANGE   = LED3 -> PD13
    // RED      = LED5 -> PD14
    // BLUE     = LED6 -> PD15

    // GPIO Ports Clock Enable
    __HAL_RCC_GPIOD_CLK_ENABLE();

    GPIO_InitTypeDef GPIO_InitStruct;

    //Configure GPIO pins : LD4_Pin LD3_Pin LD5_Pin LD6_Pin Audio_RST_Pin */
    GPIO_InitStruct.Pin = LD3_Pin | LD4_Pin | LD5_Pin | LD6_Pin;
    GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
    HAL_GPIO_Init(GPIOD, &GPIO_InitStruct);

    HAL_GPIO_WritePin(GPIOD, LD3_Pin | LD4_Pin | LD5_Pin | LD6_Pin , GPIO_PIN_RESET);

    // Initialization: GPIO
    // MX_GPIO_Init();
}


void init_push_button(void)
{
    //BSP_PB_Init(BUTTON_USER, BUTTON_MODE_EXTI); 
    BSP_PB_Init(BUTTON_KEY, BUTTON_MODE_GPIO);
}
