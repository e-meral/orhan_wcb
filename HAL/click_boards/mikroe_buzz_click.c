/***************************************************************************************************
**
** File         : mikroe_buzz_click.c
** Created on   :
** Author       : Erhan Meral
** Project      : Orhan - WCB (Wheelchair Control Box)
** Description  : A piezo buzzer has been added to the hardware for:
**              : 1) Beeps shortly when WCB get powered / turned on
**              : 2) Plays alert sounds when wheelchair drive reversely [R]
**
***************************************************************************************************/

// Includes ****************************************************************************************
#include "stm32f4xx.h"
#include "stm32f4xx_hal.h"
#include "mikroe_buzz_click.h"


// Globals *****************************************************************************************
GPIO_TypeDef* PIEZO_GPIO_PORTS = PIEZO_PLUS_PORT;
const uint16_t PIEZO_GPIO_PINS = PIEZO_PLUS_PIN;

TIM_HandleTypeDef htim1;


// Functions *************************************************************************************** 

void init_buzz_click(void)
{
    /***********************************************************
    Timer1 is used for PWM generation: A 8MHz external XTAL
    oscillator provides the clock source to the microcontroller.
    Internally the clock frequency is boosted to 168MHz (PLL).

    RCC_OscInitStruct.PLL.PLLM = 8;
    RCC_OscInitStruct.PLL.PLLN = 336;
    RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;

    System clock freq: (((8MHz) / 8) * 336) / 2 = 168 MHz

    ------------------------------------------------------------

    PWM freq: ((168MHz / 2) / 840 / 100) * 2 = 2000Hz = 2kHz 
    
    htim1.Init.Period = 100;
    sConfigOC.Pulse = 50;
    Duty-Cycle = (50/100) * 100 = 50%

    ***********************************************************/

    TIM_MasterConfigTypeDef sMasterConfig;
    TIM_OC_InitTypeDef sConfigOC;

    htim1.Instance = TIM1;
    htim1.Init.Prescaler = 839;
    htim1.Init.CounterMode = TIM_COUNTERMODE_UP;
    htim1.Init.Period = 100;
    htim1.Init.ClockDivision = TIM_CLOCKDIVISION_DIV2;
    HAL_TIM_PWM_Init(&htim1);

    sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
    sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
    HAL_TIMEx_MasterConfigSynchronization(&htim1, &sMasterConfig);

    sConfigOC.OCMode = TIM_OCMODE_PWM1;
    sConfigOC.OCIdleState = TIM_OCIDLESTATE_SET;
    sConfigOC.Pulse = 50;
    sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
    sConfigOC.OCFastMode = TIM_OCFAST_ENABLE;

    HAL_TIM_PWM_ConfigChannel(&htim1, &sConfigOC, TIM_CHANNEL_3);

    HAL_TIM_PWM_MspInit(&htim1);
}


void play_piezo_poweron_sound(void)
{
    static int flag = 1;

    if(flag)
    {
        flag = 0;
        start_piezo_sound();
    }
    else
    {
        flag = 0;
        stop_piezo_sound();
    }
}


void play_piezo_reverse_driving_sound(void)
{
    static int flag = 1;

    if(flag == 1)
    {
        flag = 0;
        start_piezo_sound();
    }
    else
    {
        flag = 1;
        stop_piezo_sound();
    }
}


void start_piezo_sound(void)
{
    HAL_TIM_Base_Start(&htim1); 
    HAL_TIM_PWM_Start(&htim1,TIM_CHANNEL_3);
}


void stop_piezo_sound(void)
{
    HAL_TIM_Base_Stop(&htim1); 
    HAL_TIM_PWM_Stop(&htim1,TIM_CHANNEL_3);
}


void HAL_TIM_PWM_MspInit(TIM_HandleTypeDef* htim_pwm)
{
    GPIO_InitTypeDef GPIO_InitStruct;

    if(htim_pwm->Instance==TIM1)
    {
        // Peripheral clock enable
        __HAL_RCC_GPIOE_CLK_ENABLE();
        __TIM1_CLK_ENABLE();

        GPIO_InitStruct.Pin = PIEZO_PLUS_PIN;
        GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
        GPIO_InitStruct.Pull = GPIO_NOPULL;
        GPIO_InitStruct.Speed = GPIO_SPEED_HIGH;
        GPIO_InitStruct.Alternate = GPIO_AF1_TIM1;
        HAL_GPIO_Init(PIEZO_PLUS_PORT, &GPIO_InitStruct);
    }
}
