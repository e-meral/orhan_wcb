/***************************************************************************************************
**
** File         : mikroe_ble_p_click.c
** Created on   :
** Author       : Erhan Meral
** Project      : Orhan - WCB (Wheelchair Control Box)
** Description  : This click board supports Bluetooth Low Energy (BLE)
**              : communication. For example to communicate with a
**              : smartphone and/or tablet. It contains a nRF8001.
**
**
***************************************************************************************************/

// Includes ****************************************************************************************
#include "stm32f4xx.h"
#include "stm32f4xx_hal.h"
#include "mikroe_ble_p_click.h"

// Functions ***************************************************************************************

void init_ble_p_click(void)
{

}
