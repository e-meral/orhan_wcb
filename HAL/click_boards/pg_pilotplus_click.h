/***************************************************************************************************
**
** File         : pg_pilotplus_click.h
** Created on   :
** Author       : Erhan Meral
** Project      : Orhan - WCB (Wheelchair Control Box)
** Description  : See below for top-view of the hardware.
**
**
**                 USB (mini)          USB (mini)        CAN-BUS
**                 (UART)              (PWR/PRG/DBG)     (GND, L, H, 3.3V)
**
**        --------|--|----------------|--|--------------|-|-|-|-|-----
**       / O                   |               |                    O \
**      |                      |  ST-link      |                      |
**      |-----------------     |  Debugger     |     -----------------|
**      | 1 - GYRO       |     | ------------- |     | 3 - CUSTOM     |
**      |     CLICK      |     |               |     |     CLICK      |
**      |                |     |               |     |                |
**      |-----------------     | STM32F4       |     -----------------|
**      |                      | DISCOVERY     |                      |
**      |-----------------     |               |     -----------------|
**      | 2 - BUZZ       |     | uC =          |     | 4 - BLE P      |
**      |     CLICK      |     | STM32F407VG   |     |     CLICK      |
**      |                |     |               |     |                |
**      |-----------------     |               |     -----------------|
**      |                      |               |                      |
**      \ O                    |               |                    O /
**       -----------------------------|--|----------------------------
**
**                                    USB (micro)
**                                    (Connected to STM32F407VG)
**
**
**              : The PG Pilot+ Motor Control Box is responsible to handle the two motors (M1 and M2) 
**              : of the wheelchair. Besides that is has a so-called J-BUS connectivity which has
**              : propierty 6-way connector on board for Joystick-communication and Actuator Control Box connectivity.
**              : The pins of the propierty 6-way connector are described below:
**
**          _
**       --| |--
**      |       |
**     /    1    \        1 - Battery[+] (~24V)
**    |  5     4  |       2 - Battery[-] (Ground)
**    |  6     3  |       3 - ? Not needed, will be checked later. For now not needed !
**     \    2    /        4 - Drive data (serial, half-duplex, 19200 baud, 8-databits, even parity)
**      |       |         5 - Enable 5V
**       -------          6 - Actuator data (custom serial communication)
**
**
***************************************************************************************************/

#ifndef __PG_PILOTPLUS_CLICK_H__
#define __PG_PILOTPLUS_CLICK_H__

// PG Pilot+ click board is clicked on MikroBus connector 3.

// Defines *****************************************************************************************

// PG Pilot+ communication lines

// The drive-data line is onw wire half-duplex UART line.
// TX pin connected to PC6 (TX USART6)
#define PG_DRIVE_TX_PIN         GPIO_PIN_6
#define PG_DRIVE_TX_PORT        GPIOC
#define PG_DRIVE_TX_GPIO_CLK    RCC_AHB1Periph_GPIOC

// RX pin connected to PC7 (RX USART6)
#define PG_DRIVE_RX_PIN         GPIO_PIN_7
#define PG_DRIVE_RX_PORT        GPIOC
#define PG_DRIVE_RX_GPIO_CLK    RCC_AHB1Periph_GPIOC

// Actuator control pin connected to PB5 (GPIO)
#define PG_ACTUATOR_PIN         GPIO_PIN_5
#define PG_ACTUATOR_PORT        GPIOB

// 5V enable pin connected to PC9 (GPIO)
#define PG_5V_EN_PIN            GPIO_PIN_9
#define PG_5V_EN_PORT           GPIOC

// TODO:
// #define PILOTPLUS_NOTKNOWN	// There is one pin on the connector that must be figured out.. ?


//--------------------------------------------------------------------------------------------------


// Driver inputs (foot and head control)

// Left head control switch is connected to PB4
#define HEAD_CONTROL_SWITCH_L_PIN   GPIO_PIN_4
#define HEAD_CONTROL_SWITCH_L_PORT  GPIOB

// Right head control switch is connected to PB3
#define HEAD_CONTROL_SWITCH_R_PIN   GPIO_PIN_3
#define HEAD_CONTROL_SWITCH_R_PORT  GPIOB

// Acceleration pedal is connected to PE5
// Driver can control the acceleration pedal with his/her right-foot (front-side of the foot)
#define FOOT_ACCELERATION_PEDAL_PIN   GPIO_PIN_5
#define FOOT_ACCELERATION_PEDAL_PORT  GPIOE

// Driver can control the dircetion of the wheelchair with his/her right-foot (back-side of the foot)
// This will toggle the direction between FORWARD and REVERSE !
#define FOOT_DIRECTION_PEDAL_PIN   GPIO_PIN_4
#define FOOT_DIRECTION_PEDAL_PORT  GPIOE


// Function prototypes *****************************************************************************

// Public functions 

void init_pg_pilotplus_click(void);


#endif
