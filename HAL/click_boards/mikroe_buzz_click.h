/***************************************************************************************************
**
** File         : mikroe_buzz_click.h
** Created on   :
** Author       : Erhan Meral
** Project      : Orhan - WCB (Wheelchair Control Box)
** Description  : See below for top-view of the hardware.
**
**
**                 USB (mini)          USB (mini)        CAN-BUS
**                 (UART)              (PWR/PRG/DBG)     (GND, L, H, 3.3V)
**
**        --------|--|----------------|--|--------------|-|-|-|-|-----
**       / O                   |               |                    O \
**      |                      |  ST-link      |                      |
**      |-----------------     |  Debugger     |     -----------------|
**      | 1 - GYRO       |     | ------------- |     | 3 - CUSTOM     |
**      |     CLICK      |     |               |     |     CLICK      |
**      |                |     |               |     |                |
**      |-----------------     | STM32F4       |     -----------------|
**      |                      | DISCOVERY     |                      |
**      |-----------------     |               |     -----------------|
**      | 2 - BUZZ       |     | uC =          |     | 4 - BLE P      |
**      |     CLICK      |     | STM32F407VG   |     |     CLICK      |
**      |                |     |               |     |                |
**      |-----------------     |               |     -----------------|
**      |                      |               |                      |
**      \ O                    |               |                    O /
**       -----------------------------|--|----------------------------
**
**                                    USB (micro)
**                                    (Connected to STM32F407VG)
**
***************************************************************************************************/

#ifndef __MIKROE_BUZZ_CLICK_H__
#define __MIKROE_BUZZ_CLICK_H__

// Defines *****************************************************************************************

// Buzz click board is clicked on MikroBus connector 2.
// Piezo pins are connected to PE13 and ground.
#define PIEZO_PLUS_PIN          GPIO_PIN_13
#define PIEZO_PLUS_PORT         GPIOE
#define PIEZO_PLUS_GPIO_CLK     RCC_AHB1Periph_GPIOE

// Connected to Ground, for now this will be not used
//#define PIEZO_MIN_PIN          GPIO_PIN_14
//#define PIEZO_MIN_PORT         GPIOB
//#define PIEZO_MIN_GPIO_CLK     RCC_AHB1Periph_GPIOB
void HAL_TIM_PWM_MspInit(TIM_HandleTypeDef *htim);

// Function prototypes *****************************************************************************
void init_buzz_click(void);
void play_piezo_poweron_sound(void);
void play_piezo_reverse_driving_sound(void);
void start_piezo_sound(void);
void stop_piezo_sound(void);

#endif
