/***************************************************************************************************
**
** File         : mikroe_gyro_click.c
** Created on   :
** Author       : Erhan Meral
** Project      : Orhan - WCB (Wheelchair Control Box)
** Description  : This click board contains a Gyroscope (L3GD20).
**
***************************************************************************************************/

// Includes ****************************************************************************************
#include "stm32f4xx.h"
#include "stm32f4xx_hal.h"
#include "mikroe_gyro_click.h"
