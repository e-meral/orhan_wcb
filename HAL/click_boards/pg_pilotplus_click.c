/***************************************************************************************************
**
** File         : pg_pilotplus_click.c
** Created on   :
** Author       : Erhan Meral
** Project      : Orhan - WCB (Wheelchair Control Box)
** Description  : The pg_pilotplus_communication_click is a custom click-board designed to handle the
**              : communication with the PG Motor Control Box. This communication cable exists about
**              : six wires. Besides the communication, this board also handles the driver input signals
**              : like foot pedal inputs and head control inputs. The click board contains:
**              : ----------------------------------------------------------------------------------
**              : 1) DC/DC converter to convert the battery voltage (~ 24-30V) to 5V (board voltage).
**              : 2) Short-circuit protection
**              : 3) Level-converter for Motor Control Box communication signals ( 5V <--> 3.3V )
**              : 4) Digital inputs (foot and head control switches)
**
**
***************************************************************************************************/

// Includes ****************************************************************************************
#include "stm32f4xx.h"
#include "stm32f4xx_hal.h"
#include "pg_pilotplus_click.h"


// Globals *****************************************************************************************


// Functions *************************************************************************************** 

void init_pg_pilotplus_click(void)
{
    __USART6_CLK_ENABLE();
    __GPIOB_CLK_ENABLE();
    __GPIOC_CLK_ENABLE();
    __GPIOE_CLK_ENABLE();
    
    GPIO_InitTypeDef GPIO_InitStructure;


    // PG Pilot+ has in total 4 communication lines with the joystick.

    // The drive communication line uses half-duplex UART communication.
    // Drive communicacation pin: configure Alternate Function for TX and RX:
    GPIO_InitStructure.Pin = PG_DRIVE_TX_PIN;
    GPIO_InitStructure.Mode = GPIO_MODE_AF_PP;
    GPIO_InitStructure.Alternate = GPIO_AF8_USART6;
    GPIO_InitStructure.Speed = GPIO_SPEED_HIGH;
    GPIO_InitStructure.Pull = GPIO_NOPULL;
    HAL_GPIO_Init(PG_DRIVE_TX_PORT, &GPIO_InitStructure);

    GPIO_InitStructure.Pin = PG_DRIVE_RX_PIN;
    GPIO_InitStructure.Mode = GPIO_MODE_AF_OD;
    HAL_GPIO_Init(PG_DRIVE_RX_PORT, &GPIO_InitStructure);

    // Actuator communication pin: Output
    GPIO_InitStructure.Pin = PG_ACTUATOR_PIN;
    GPIO_InitStructure.Mode = GPIO_MODE_OUTPUT_PP;
    GPIO_InitStructure.Pull = GPIO_NOPULL;
    GPIO_InitStructure.Speed = GPIO_SPEED_FREQ_LOW;
    HAL_GPIO_Init(PG_ACTUATOR_PORT, &GPIO_InitStructure);
    HAL_GPIO_WritePin(PG_5V_EN_PORT, PG_5V_EN_PIN, GPIO_PIN_RESET);

    // Enable pin which is '1' (high = 5V) during communication 
    GPIO_InitStructure.Pin = PG_5V_EN_PIN;
    GPIO_InitStructure.Mode = GPIO_MODE_OUTPUT_PP;
    GPIO_InitStructure.Pull = GPIO_NOPULL;
    GPIO_InitStructure.Speed = GPIO_SPEED_FREQ_LOW;
    HAL_GPIO_Init(PG_5V_EN_PORT, &GPIO_InitStructure);
    HAL_GPIO_WritePin(PG_ACTUATOR_PORT, PG_ACTUATOR_PIN, GPIO_PIN_SET);

    // There is a 4th pin which is not used for now.
    // TODO: This pins funcationality must be figured out. 

}
