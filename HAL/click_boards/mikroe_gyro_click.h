/***************************************************************************************************
**
** File         : mikroe_gyro_click.h
** Created on   :
** Author       : Erhan Meral
** Project      : Orhan - WCB (Wheelchair Control Box)
** Description  : See below for top-view of the hardware.
**
**
**                 USB (mini)          USB (mini)        CAN-BUS
**                 (UART)              (PWR/PRG/DBG)     (GND, L, H, 3.3V)
**
**        --------|--|----------------|--|--------------|-|-|-|-|-----
**       / O                   |               |                    O \
**      |                      |  ST-link      |                      |
**      |-----------------     |  Debugger     |     -----------------|
**      | 1 - GYRO       |     | ------------- |     | 3 - CUSTOM     |
**      |     CLICK      |     |               |     |     CLICK      |
**      |                |     |               |     |                |
**      |-----------------     | STM32F4       |     -----------------|
**      |                      | DISCOVERY     |                      |
**      |-----------------     |               |     -----------------|
**      | 2 - BUZZ       |     | uC =          |     | 4 - BLE P      |
**      |     CLICK      |     | STM32F407VG   |     |     CLICK      |
**      |                |     |               |     |                |
**      |-----------------     |               |     -----------------|
**      |                      |               |                      |
**      \ O                    |               |                    O /
**       -----------------------------|--|----------------------------
**
**                                    USB (micro)
**                                    (Connected to STM32F407VG)
**
***************************************************************************************************/

#ifndef __MIKROE_GYRO_CLICK_H__
#define __MIKROE_GYRO_CLICK_H__

// Defines *****************************************************************************************

// Gyro click board is clicked on MikroBus connector 1.


#endif
