/***************************************************************************************************
**
** File         : hal_stm32f4discovery_board.h
** Created on   :
** Author       : Erhan Meral
** Project      : Orhan - WCB (Wheelchair Control Box)
** Description  :
**
**
***************************************************************************************************/

#ifndef __HAL_STM32F4DISCOVERY_BOARD_H_
#define __HAL_STM32F4DISCOVERY_BOARD_H_


// Function prototypes *****************************************************************************

#define LEDn                             	4

#define LED_RED_PIN                         GPIO_PIN_14
#define LED_RED_GPIO_PORT                   GPIOD
#define LED_RED_GPIO_CLK                    RCC_AHB1Periph_GPIOD

#define LED_GREEN_PIN                       GPIO_PIN_12
#define LED_GREEN_GPIO_PORT                 GPIOD
#define LED_GREEN_GPIO_CLK                  RCC_AHB1Periph_GPIOD

#define LED_BLUE_PIN                        GPIO_PIN_15
#define LED_BLUE_GPIO_PORT                  GPIOD
#define LED_BLUE_GPIO_CLK                   RCC_AHB1Periph_GPIOD

#define LED_ORANGE_PIN                      GPIO_PIN_13
#define LED_ORANGE_GPIO_PORT                GPIOD
#define LED_ORANGE_GPIO_CLK                 RCC_AHB1Periph_GPIOD

#define LD4_Pin GPIO_PIN_12
#define LD4_GPIO_Port GPIOD
#define LD3_Pin GPIO_PIN_13
#define LD3_GPIO_Port GPIOD
#define LD5_Pin GPIO_PIN_14
#define LD5_GPIO_Port GPIOD
#define LD6_Pin GPIO_PIN_15
#define LD6_GPIO_Port GPIOD

typedef enum
{
  LED_GREEN     = 0,
  LED_BLUE      = 1,
  LED_RED       = 2,
  LED_ORANGE    = 3
}Led_TypeDef2;

void init_stm32f4discovery_board(void);

#endif
