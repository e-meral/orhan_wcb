/***************************************************************************************************
**
** File         : hal_error.h
** Created on   :
** Author       : Erhan Meral
** Project      : Orhan - WCB (Wheelchair Control Box)
** Description  :
**
**
***************************************************************************************************/


#ifndef __HAL_ERROR_H_
#define __HAL_ERROR_H_

// Function protoypes ******************************************************************************

void Error_Handler(void);

#endif
