/***************************************************************************************************
**
** File         : pg_pilotplus_communication.h
** Created on   : 
** Author       : Erhan Meral
** Project      : Orhan - WCB (Wheelchair Control Box)
** Description  : The PilotPlus Motor Control Box communication.
**
**
***************************************************************************************************/

#ifndef __PG_PILOTPLUS_COMMUNICATION_H__
#define __PG_PILOTPLUS_COMMUNICATION_H__

#include <stdint.h>

// Communication ***********************************************************************************

// Drive data message

// Start-byte, drive data message start always with the addres of the Pilot+ Motor Control Box which is: 0x6A
#define PG_PILOTPLUS_START_BYTE 	    0x6A


// Drive data (Negative = REVERSE, 0 = NO_REVERSE and NO_FORWARD, Positive = FORWARD)  
#define PG_PILOTPLUS_DRIVE_VALUE_MIN    -100	// 0x9C = -100 (two's complement) 
#define PG_PILOTPLUS_DRIVE_VALUE_MAX     100	// 0x64 =  100


// Turn data (Negative = LEFT, 0 = NO_LEFT and NO_RIGHT, Positive = RIGHT) 
#define PG_PILOTPLUS_TURN_VALUE_MIN     -100	// 0x9C = -100 (two's complement)
#define PG_PILOTPLUS_TURN_VALUE_MAX      100	// 0x64 =  100


// Speed levels, the PG Pilot+ Motor Control Box supports in total 5 speed levels [1 - 5]
#define PG_PILOTPLUS_SPEED1_DATA		0x00    // Speed 1
#define PG_PILOTPLUS_SPEED2_DATA		0x03    // Speed 2
#define PG_PILOTPLUS_SPEED3_DATA		0x06    // Speed 3
#define PG_PILOTPLUS_SPEED4_DATA		0x09    // Speed 4
#define PG_PILOTPLUS_SPEED5_DATA		0x0C    // Speed 5

#define PG_PILOTPLUS_SPEED_LEVEL_MIN    1
#define PG_PILOTPLUS_SPEED_LEVEL_MAX    5


// TODO: the purpose of this byte is not known for now -> TBD
#define PILOTPLUS_TBD_BYTE              0x00    // Just for now, keep it 0x00


// Actuator data message, controlling the actuators like: 
// Lights, turn-indicators and different linair actuators will be happen through the Pilot+ module
// which communicates with the Actuator Control Box. 

#define PG_PILOTPLUS_ACTUATOR1_DOWN     (0x0001 << 0)   // Seat down/up.    "01" = down, "10" = up, "00" and "11" = stop
#define PG_PILOTPLUS_ACTUATOR1_UP       (0x0001 << 1)   // Seat down/up.    "01" = down, "10" = up, "00" and "11" = stop
#define PG_PILOTPLUS_ACTUATOR2_DOWN     (0x0001 << 2)   // Back tilt.       "01" = down, "10" = up, "00" and "11" = stop
#define PG_PILOTPLUS_ACTUATOR2_UP       (0x0001 << 3)   // Back tilt.       "01" = down, "10" = up, "00" and "11" = stop
#define PG_PILOTPLUS_NOTUSED_FIELD1     (0x0001 << 4)   // TODO: ? 
#define PG_PILOTPLUS_NOTUSED_FIELD2     (0x0001 << 5)   // TODO: ? 
#define PG_PILOTPLUS_ACTUATOR3_DOWN     (0x0001 << 6)   // "01" = down, "10" = up, "00" and "11" = stop
#define PG_PILOTPLUS_ACTUATOR3_UP       (0x0001 << 7)   // "01" = down, "10" = up, "00" and "11" = stop

#define PG_PILOTPLUS_NOTUSED_FIELD3     (0x0001 << 8)   // TODO: ACTUATOR4_DOWN ? 
#define PG_PILOTPLUS_NOTUSED_FIELD4     (0x0001 << 9)   // TODO: ACTUATOR4_UP ?
#define PG_PILOTPLUS_DRIVE_RELAY        (0x0001 << 10)
#define PG_PILOTPLUS_LIGHTS             (0x0001 << 11)  // '0' = lights off, '1' = lights on
#define PG_PILOTPLUS_NOTUSED_FIELD5     (0x0001 << 12)  // TODO: ACTUATOR5_DOWN ?
#define PG_PILOTPLUS_NOTUSED_FIELD6     (0x0001 << 13)  // TODO: ACTUATOR5_UP ?
#define PG_PILOTPLUS_INDICATOR_LIGHT_L  (0x0001 << 14)  // '1' = Left direction indicator (light) blinks, '0' = off
#define PG_PILOTPLUS_INDICATOR_LIGHT_R  (0x0001 << 15)  // '1' = Right direction indicator (light) blinks, '0' = off


typedef enum
{
    START_ADDRESS_BYTE_NUMBER   = 0,
    DRIVE_VALUE_BYTE_NUMBER     = 1,
    TURN_VALUE_BYTE_NUMBER      = 2,
    SPEED_LEVEL_BYTE_NUMBER     = 3,
    TBD_BYTE_NUMBER             = 4,
    CHECKSUM_BYTE_NUMBER        = 5,

    DRIVE_MESSAGE_BYTE_LENGTH   = 6
}drive_message_bytes;


// Function prototypes *****************************************************************************

// Public functions 

void init_pg_pilotplus_communication(void);
bool set_drive_level(int8_t drive_value);
bool set_turn_value(int8_t turn_value);
bool set_speed_level(uint8_t speed_level);
bool send_drive_data(void);
uint8_t get_speed_level(void);


#endif
