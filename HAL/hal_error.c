/***************************************************************************************************
**
** File         : hal_error.c
** Created on   :
** Author       : Erhan Meral
** Project      : Orhan - WCB (Wheelchair Control Box)
** Description  :
**
**
***************************************************************************************************/


void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */

  /* USER CODE END Error_Handler_Debug */
}
