/***************************************************************************************************
**
** File         : hal_clock.h
** Created on   :
** Author       : Erhan Meral
** Project      : Orhan - WCB (Wheelchair Control Box)
** Description  :
**
**
***************************************************************************************************/

#ifndef __HAL_CLOCK_H_
#define __HAL_CLOCK_H_


// Function prototypes *****************************************************************************

void init_system_clock(void);

#endif
