/***************************************************************************************************
**
** File         : pg_pilotplus_communication.c
** Created on   :
** Author       : Erhan Meral
** Project      : Orhan - WCB (Wheelchair Control Box)
** Description  :
**
**
***************************************************************************************************/

// Includes
#include <stdbool.h>
#include <string.h>
#include "stm32f4xx_hal.h"
#include "stm32f4xx_hal_uart.h"
#include "stm32f4xx_hal_gpio.h"
#include "pg_pilotplus_click.h"
#include "pg_pilotplus_communication.h"

// Globals *****************************************************************************************

const uint8_t speeds_table[PG_PILOTPLUS_SPEED_LEVEL_MAX] =
{
    PG_PILOTPLUS_SPEED1_DATA,
    PG_PILOTPLUS_SPEED2_DATA,
    PG_PILOTPLUS_SPEED3_DATA,
    PG_PILOTPLUS_SPEED4_DATA,
    PG_PILOTPLUS_SPEED5_DATA
};


uint8_t pg_send_buffer[DRIVE_MESSAGE_BYTE_LENGTH] = {PG_PILOTPLUS_START_BYTE,0,0,0,0,0};

uint8_t current_drive_value = 0;

uint8_t current_turn_value = 0;


// For safety reasons, start always with the lowest speed level
uint8_t current_speed_level = PG_PILOTPLUS_SPEED_LEVEL_MIN+1;

uint16_t current_actuator_message = 0;


static UART_HandleTypeDef s_UARTHandle;
static TIM_HandleTypeDef htim2;


// Function prototypes *****************************************************************************

// Local functions

uint8_t calculate_drive_message_checksum(void);
void init_timer2(void);
void TIM2_IRQHandler(void);
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim);


// Functions *************************************************************************************** 

void init_pg_pilotplus_communication(void)
{
    // Baudrate: 19200, 8 databits and even parity
    s_UARTHandle.Instance        = USART6;              // USART6 is connected to the TX/RX pins on MikroBus connector 3
    s_UARTHandle.Init.BaudRate   = 19200;               // Baud rate
    s_UARTHandle.Init.WordLength = UART_WORDLENGTH_9B;  // WordLength must be 9 because of even-parity-bit 
    s_UARTHandle.Init.StopBits   = UART_STOPBITS_1;
    s_UARTHandle.Init.Parity     = UART_PARITY_EVEN;
    s_UARTHandle.Init.HwFlowCtl  = UART_HWCONTROL_NONE;
    s_UARTHandle.Init.Mode       = UART_MODE_TX_RX;

    if (HAL_UART_Init(&s_UARTHandle) != HAL_OK)
    {
        asm("bkpt 255");
    }

    //pg_send_buffer[START_ADDRESS_BYTE_NUMBER] = PG_PILOTPLUS_START_BYTE;
    //pg_send_buffer[SPEED_LEVEL_BYTE_NUMBER] = speeds_table[PG_PILOTPLUS_SPEED_LEVEL_MIN-1];

    init_timer2();
}


bool set_drive_level(int8_t drive_value)
{
    if( (drive_value < PG_PILOTPLUS_DRIVE_VALUE_MIN) && (drive_value > PG_PILOTPLUS_DRIVE_VALUE_MAX) )
    {
        // Every selection outside this scope will be ignored.
        return false;
    }
    else
    {
        pg_send_buffer[DRIVE_VALUE_BYTE_NUMBER] = drive_value;
        return true;
    }
}


bool set_turn_value(int8_t turn_value)
{
    if( (current_turn_value < PG_PILOTPLUS_TURN_VALUE_MIN) && (current_turn_value > PG_PILOTPLUS_TURN_VALUE_MAX) )
    {
        // Every selection outside this scope will be ignored.
        return false;
    }
    else
    {
        pg_send_buffer[TURN_VALUE_BYTE_NUMBER] = turn_value;
        return true;
    }
}


bool set_speed_level(uint8_t speed_level)
{
    if( (speed_level < PG_PILOTPLUS_SPEED_LEVEL_MIN) && (speed_level > PG_PILOTPLUS_SPEED_LEVEL_MAX) )
    {
        // The PG Pilot+ module supports only 5 speed levels [1 - 5]
        // Every selection outside this scope will be ignored.
        return false;
    }
    else
    {
        pg_send_buffer[SPEED_LEVEL_BYTE_NUMBER-1] = speeds_table[speed_level];
        return true;
    }
}


uint8_t get_speed_level(void)
{
    return current_speed_level;
}


bool send_drive_data(void)
{

    HAL_GPIO_WritePin(PG_5V_EN_PORT, PG_5V_EN_PIN, GPIO_PIN_SET);
      
    pg_send_buffer[CHECKSUM_BYTE_NUMBER] = calculate_drive_message_checksum();

    HAL_UART_Transmit(&s_UARTHandle, pg_send_buffer, sizeof(pg_send_buffer), HAL_MAX_DELAY);

}


uint8_t calculate_drive_message_checksum(void)
{
    return (0xFF) - ( pg_send_buffer[START_ADDRESS_BYTE_NUMBER] + 
                      pg_send_buffer[DRIVE_VALUE_BYTE_NUMBER] +
                      pg_send_buffer[TURN_VALUE_BYTE_NUMBER] + 
                      pg_send_buffer[SPEED_LEVEL_BYTE_NUMBER] +
                      pg_send_buffer[TBD_BYTE_NUMBER]
                    );
}


/***************************************************************************************************

Actuator Controlling:

The actuator data message is a custom serial protocol. The message consists of 16-bit data.
Each bit represents a action flag, like: TURN_LIGHT_ON. See header file for more information.
This message needs to be send every 15ms !

                         ~bit-value            ~bit-value                   ~bit-value
 5V ----          -------------          -------------                -------------          ------- IDLE ----//-------
       |          |     |     |          |     |     |                |     |     |          |
       |          |     |  X  |          |     |  X  |                |     |  X  |          |
       |          |     |     |          |     |     |                |     |     |          |
 0V    ------------     ------------------     -------- // ------------     ------------------ 

          320us    160us 160us   320us    160us 160us         320us    160us 160us
                         
       |<-------------------->|<-------------------->|  // |<-------------------->|<-------->|
                640us                   640us                       640us            320us

                bit-0                   bit-1                       bit-15




***************************************************************************************************/

// To generate the Actuator message signal, Timer2 of the uC has been used.
void init_timer2(void)
{
    /***********************************************************
    
    Timer2 is used for Delay function (us).

    168MHz / 4 = 42MHz
    42MHz / 42000 = 1MHz

    The first interrupt will happen 320us (just a value) after initialization of Timer2.
    The state-machine inside the timer-interrupt function will decide every time the next interrupt
    interval!

    ***********************************************************/
    
    HAL_GPIO_WritePin(PG_ACTUATOR_PORT, PG_ACTUATOR_PIN, GPIO_PIN_SET);

    __HAL_RCC_TIM2_CLK_ENABLE();
    __TIM2_CLK_ENABLE();

    htim2.Instance = TIM2;
    htim2.Init.Prescaler = (168/2)-1;
    htim2.Init.CounterMode = TIM_COUNTERMODE_UP;
    htim2.Init.Period = 320;
    htim2.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
    htim2.Init.RepetitionCounter = 0;    
    HAL_TIM_PWM_Init(&htim2);

    // Start Timer2
    HAL_TIM_Base_Start_IT(&htim2);

    HAL_NVIC_SetPriority(TIM2_IRQn, 4, 1);
    HAL_NVIC_EnableIRQ(TIM2_IRQn);
}


void TIM2_IRQHandler(void)
{
    HAL_TIM_IRQHandler(&htim2);
}


// Actuator message signal handler (interrupt driven)
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
    static int bit_number = 0;
    static int actuator_bit_part = 1;

    unsigned int current_actuator_message = PG_PILOTPLUS_DRIVE_RELAY; // DRIVE RELAY must be active in the Actuator message for driving the wheelchair

    switch(actuator_bit_part)
    {
        case 1: actuator_bit_part = 2;
                HAL_GPIO_WritePin(PG_ACTUATOR_PORT, PG_ACTUATOR_PIN, GPIO_PIN_RESET);
                __HAL_TIM_SET_AUTORELOAD(&htim2, 320);                                
                break;

        case 2: actuator_bit_part = 3;
                HAL_GPIO_WritePin(PG_ACTUATOR_PORT, PG_ACTUATOR_PIN, GPIO_PIN_SET);
                __HAL_TIM_SET_AUTORELOAD(&htim2, 160);                                
                break;

        case 3: actuator_bit_part = 1;

                if(~current_actuator_message & (0x8000>>bit_number))
                {
                    HAL_GPIO_WritePin(PG_ACTUATOR_PORT, PG_ACTUATOR_PIN, GPIO_PIN_SET);
                }
                else
                {
                    HAL_GPIO_WritePin(PG_ACTUATOR_PORT, PG_ACTUATOR_PIN, GPIO_PIN_RESET);
                }
                                
                __HAL_TIM_SET_AUTORELOAD(&htim2, 160);

                bit_number++;
                if(bit_number > 15)
                {
                    bit_number = 0;
                    actuator_bit_part = 4;
                }
                break;

        case 4: actuator_bit_part = 1;
                HAL_GPIO_WritePin(PG_ACTUATOR_PORT, PG_ACTUATOR_PIN, GPIO_PIN_SET);
                __HAL_TIM_SET_AUTORELOAD(&htim2, 4440);
                break;

        
        default: break;
    }
}
